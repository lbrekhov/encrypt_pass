package encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class NewEncryptPass {

    private static String strkey ="Blowfish";
    private static Base64 base64 = new Base64(true);

    public static void main(String[] args) throws Exception {

        if (args[0].startsWith("propertyKey=") && args[1].startsWith("propertyValue=")) {
            String propertyKey = args[0].substring(12);
            System.out.println(propertyKey);
            String propertyPassword = encrypt(args[1].substring(14));
            setProperty(propertyKey, propertyPassword);
        }
    }

    public static void setProperty(String passwordKey, String password) {
        Properties prop = new Properties();
        OutputStream output;

        try {
            output = new FileOutputStream("src/main/resources/some.properties");
            prop.setProperty("username", "lol");
            prop.setProperty(passwordKey, password);
            prop.store(output, null);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String encrypt(String Data)throws Exception{

        SecretKeySpec key = new SecretKeySpec(strkey.getBytes("UTF8"), "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return base64.encodeToString(cipher.doFinal(Data.getBytes("UTF8")));
    }

    public static String decrypt(String encrypted)throws Exception{

        byte[] encryptedData = base64.decodeBase64(encrypted);
        SecretKeySpec key = new SecretKeySpec(strkey.getBytes("UTF8"), "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decrypted = cipher.doFinal(encryptedData);

        return new String(decrypted);
    }
}
